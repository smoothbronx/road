import {
    ValidatorConstraintInterface,
    ValidationArguments,
    ValidatorConstraint,
    registerDecorator,
    ValidationOptions,
} from 'class-validator';

export function IsPasswordsMatch(validationOptions?: ValidationOptions) {
    return (object: any, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsPasswordsMatchConstraint,
        });
    };
}

@ValidatorConstraint({ name: 'IsPasswordsMatch' })
export class IsPasswordsMatchConstraint
    implements ValidatorConstraintInterface
{
    public validate(value: any, args: ValidationArguments) {
        const relatedValue = (args.object as any)['passwordRepeated'];
        return JSON.stringify(value) === JSON.stringify(relatedValue);
    }
}
