import {
    ValidatorConstraintInterface,
    ValidatorConstraint,
    registerDecorator,
    ValidationOptions,
} from 'class-validator';

export function IsValidFullname(validationOptions?: ValidationOptions) {
    return function (object: object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: {
                ...validationOptions,
                message: validationOptions
                    ? validationOptions.message
                    : 'Incorrect user fullname',
            },
            constraints: [],
            validator: IsValidFullnameConstraint,
        });
    };
}

@ValidatorConstraint({ name: 'IsValidFullnameConstraint' })
export class IsValidFullnameConstraint implements ValidatorConstraintInterface {
    public validate(fullname: string) {
        return [2, 3].includes(fullname.split(' ').length);
    }
}
