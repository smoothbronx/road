import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

export const Token = createParamDecorator(
    (data, context: ExecutionContext): string => {
        function getToken(header: string): string {
            return (header ? header.split(' ').at(-1)! : '').toString();
        }

        const request: Request = context.switchToHttp().getRequest();
        return getToken(request.headers.authorization!);
    },
);
