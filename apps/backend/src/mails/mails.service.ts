import { MailConfigWorkspace } from '@/config/workspaces/mail.workspace';
import { MailerService } from '@nestjs-modules/mailer';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';

@Injectable()
export class MailsService {
    constructor(
        @Inject(MailerService)
        private readonly mailerService: MailerService,
        @Inject(MailConfigWorkspace.KEY)
        private readonly mailConfig: ConfigType<typeof MailConfigWorkspace>,
    ) {}

    public async sendMail(receiver: string, subject: string, text: string) {
        await this.mailerService.sendMail({
            to: receiver,
            from: this.mailConfig.sender.login,
            subject,
            text,
        });
    }
}
