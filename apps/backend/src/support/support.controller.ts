import { SupportService } from '@/support/support.service';
import { SupportRequestDto } from '@/support/support.dto';
import {
    HttpStatus,
    Controller,
    HttpCode,
    Inject,
    Body,
    Post,
} from '@nestjs/common';
import {
    ApiCreatedResponse,
    ApiOperation,
    ApiBody,
    ApiTags,
} from '@nestjs/swagger';

@ApiTags('Поддержка')
@Controller('/support/')
export class SupportController {
    constructor(
        @Inject(SupportService)
        private readonly supportService: SupportService,
    ) {}

    @ApiBody({ type: SupportRequestDto, description: 'Данные заявки' })
    @ApiOperation({ summary: 'Отправка заявки на поддержку' })
    @ApiCreatedResponse({ description: 'Заявка успешно отправлена' })
    @HttpCode(HttpStatus.CREATED)
    @Post('/')
    public sendSupportRequest(
        @Body() credentials: SupportRequestDto,
    ): Promise<void> {
        return this.supportService.sendSupportRequest(credentials);
    }
}
