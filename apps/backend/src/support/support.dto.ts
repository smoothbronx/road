import { IsValidFullname } from '@/shared/validators/fullname.validator';
import { IsEmail, IsPhoneNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SupportRequestDto {
    @IsString()
    @IsValidFullname()
    @ApiProperty({
        name: 'fullname',
        description: 'ФИО пользователя',
        example: 'Иванов Иван Иванович',
        writeOnly: true,
        required: true,
    })
    public fullname: string;

    @IsString()
    @IsEmail()
    @ApiProperty({
        name: 'email',
        description: 'Адрес электронной почты пользователя',
        example: 'smoothbronx@xenofium.com',
        writeOnly: true,
        required: true,
    })
    public email: string;

    @IsString()
    @IsPhoneNumber()
    @ApiProperty({
        name: 'phone',
        description: 'Номер телефона пользователя',
        example: '+78005553535',
        writeOnly: true,
        required: true,
    })
    public phone: string;

    @IsString()
    @ApiProperty({
        name: 'message',
        description: 'Сообщение',
        example: 'Помогите!!..',
        writeOnly: true,
        required: true,
    })
    public message: string;
}
