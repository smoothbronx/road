import { Inject, Injectable } from '@nestjs/common';
import { MailsService } from '@/mails/mails.service';
import { SupportRequestDto } from '@/support/support.dto';
import { MailConfigWorkspace } from '@/config/workspaces/mail.workspace';
import { ConfigType } from '@nestjs/config';

@Injectable()
export class SupportService {
    constructor(
        @Inject(MailsService)
        private readonly mailsService: MailsService,
        @Inject(MailConfigWorkspace.KEY)
        private readonly mailConfig: ConfigType<typeof MailConfigWorkspace>,
    ) {}

    public sendSupportRequest(
        requestCredentials: SupportRequestDto,
    ): Promise<void> {
        return this.mailsService.sendMail(
            this.mailConfig.receiver.login,
            this.getSubject(requestCredentials.email),
            this.getMessageText(requestCredentials),
        );
    }

    public getSubject(email: string): string {
        return `Новое обращение к поддержке <${email}>`;
    }

    public getMessageText(credentials: SupportRequestDto): string {
        return (
            `Пользователь: ${credentials.fullname}\n` +
            `Адрес почты: ${credentials.email}\n` +
            `Телефон: ${credentials.phone}\n\n` +
            `${credentials.message}`
        );
    }
}
