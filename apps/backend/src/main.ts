import { ApplicationConfigWorkspace } from '@/config/workspaces/application.workspace';
import { TransformInterceptor } from '@/shared/interceptors/transform.interceptor';
import { SwaggerCustomOptions, SwaggerModule } from '@nestjs/swagger';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { SwaggerOptions } from '@/swagger/swagger.options';
import { swaggerConfig } from '@/swagger/swagger.config';
import { ConfigType } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
    const application = await NestFactory.create(AppModule);

    const config: ConfigType<typeof ApplicationConfigWorkspace> =
        application.get(ApplicationConfigWorkspace.KEY);

    application.setGlobalPrefix('/api/');

    if (!['production', 'test'].includes(config.global.mode)) {
        enableSwagger(application, '/api/docs');
    }

    application.useGlobalPipes(
        new ValidationPipe({
            transform: true,
            transformOptions: { enableImplicitConversion: true },
            validateCustomDecorators: true,
        }),
    );

    application.useGlobalInterceptors(new TransformInterceptor());

    await application.listen(config.global.port);
}

function enableSwagger(application: INestApplication, path: string) {
    const document = SwaggerModule.createDocument(application, swaggerConfig);
    const options = new SwaggerOptions();
    options.setDarkTheme();

    SwaggerModule.setup(path, application, document, {
        swaggerOptions: { supportedSubmitMethods: [] },
        ...(options as SwaggerCustomOptions),
    });
}

bootstrap();
