import { DatabaseConfigWorkspace } from '@/config/workspaces/database.workspace';
import { getConfigurationModule } from '@/config/config.module';
import { ConfigModule, ConfigType } from '@nestjs/config';
import { SupportModule } from '@/support/support.module';
import { UsersModule } from '@/users/users.module';
import { UserEntity } from '@/users/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '@/auth/auth.module';
import { Module } from '@nestjs/common';

@Module({
    imports: [
        getConfigurationModule(),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule.forFeature(DatabaseConfigWorkspace)],
            inject: [DatabaseConfigWorkspace.KEY],
            useFactory: (
                config: ConfigType<typeof DatabaseConfigWorkspace>,
            ) => ({
                type: 'postgres',
                ...config,
                extra: {
                    ssl: false,
                },
                ssl: {
                    rejectUnauthorized: false,
                },
                entities: [UserEntity],
                autoLoadEntities: true,
                synchronize: true,
            }),
        }),
        SupportModule,
        UsersModule,
        AuthModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
