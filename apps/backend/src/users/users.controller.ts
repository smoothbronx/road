import { ApiException } from '@nanogiants/nestjs-swagger-api-exception-decorator';
import { UserDetailingDto } from '@/users/dto/detailing.dto';
import { UserUpdatingDto } from '@/users/dto/updating.dto';
import { User } from '@/shared/handlers/user.handler';
import { JwtAuthGuard } from '@/shared/jwt/jwt.guard';
import { UsersService } from '@/users/users.service';
import { UserEntity } from '@/users/user.entity';
import {
    UnauthorizedException,
    ConflictException,
    Controller,
    HttpStatus,
    UseGuards,
    HttpCode,
    Inject,
    Patch,
    Body,
    Get,
} from '@nestjs/common';
import {
    ApiNoContentResponse,
    ApiOkResponse,
    ApiOperation,
    ApiHeader,
    ApiTags,
    ApiBody,
} from '@nestjs/swagger';

@ApiHeader({
    name: 'Authorization',
    description: 'Bearer access token',
    example: 'Bearer <accessToken>',
    required: true,
})
@ApiException(() => new UnauthorizedException('Invalid access token'), {
    description: 'Пользователь ввел некорректный токен доступа',
})
@ApiTags('Пользователи')
@UseGuards(JwtAuthGuard)
@Controller('/users/')
export class UsersController {
    constructor(
        @Inject(UsersService)
        private readonly usersService: UsersService,
    ) {}

    @ApiOperation({ summary: 'Получение информации о пользователе' })
    @ApiOkResponse({
        description: 'Пользователь получает информацию о себе',
        type: UserDetailingDto,
    })
    @Get('/self/')
    public getSelfInformation(@User() user: UserEntity): UserEntity {
        return this.usersService.getSelfInformation(user);
    }

    @ApiOperation({ summary: 'Обновление информации о пользователе' })
    @ApiNoContentResponse({
        description: 'Пользователь успешно обновляет информацию о себе',
    })
    @ApiException(
        () => new ConflictException('User with this credentials exist'),
        {
            description:
                'Пользователь с такими реквизитами (почта или ник) уже существует',
        },
    )
    @ApiBody({ type: UserUpdatingDto, description: 'Обновленные данные' })
    @HttpCode(HttpStatus.NO_CONTENT)
    @Patch('/self/')
    public updateSelfInformation(
        @User() user: UserEntity,
        @Body() credentials: UserUpdatingDto,
    ): Promise<void> {
        return this.usersService.updateSelfInformation(user, credentials);
    }
}
