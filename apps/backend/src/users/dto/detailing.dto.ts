import { ApiProperty } from '@nestjs/swagger';

export class UserDetailingDto {
    @ApiProperty({
        name: 'id',
        description: 'Уникальный идентификатор пользователя',
        nullable: false,
        readOnly: true,
    })
    public id: string;

    @ApiProperty({
        name: 'email',
        description: 'Адрес электронной почты пользователя',
        nullable: false,
        required: true,
        readOnly: true,
    })
    public email: string;

    @ApiProperty({
        name: 'nickname',
        description: 'Ник пользователя в системе',
        nullable: false,
        required: true,
        readOnly: true,
    })
    public nickname: string;

    @ApiProperty({
        name: 'name',
        description: 'Имя пользователя в системе',
        required: false,
        readOnly: true,
    })
    public name?: string;

    @ApiProperty({
        name: 'phone',
        description: 'Номер телефона пользователя',
        required: false,
        readOnly: true,
    })
    public phone?: string;

    @ApiProperty({
        name: 'address',
        description: 'Адрес пользователя',
        required: false,
        readOnly: true,
    })
    public address?: string;

    @ApiProperty({
        name: 'city',
        description: 'Город проживания пользователя',
        required: false,
        readOnly: true,
    })
    public city?: string;

    @ApiProperty({
        name: 'street',
        description: 'Улица проживания пользователя',
        required: false,
        readOnly: true,
    })
    public street?: string;

    @ApiProperty({
        name: 'apartment',
        description: 'Номер квартиры пользователя',
        required: false,
        readOnly: true,
    })
    public apartment?: string;
}
