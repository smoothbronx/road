import { IsEmail, IsOptional, IsPhoneNumber, IsString } from 'class-validator';
import { IsValidFullname } from '@/shared/validators/fullname.validator';
import { ApiProperty } from '@nestjs/swagger';

export class UserUpdatingDto {
    @IsEmail()
    @IsOptional()
    @ApiProperty({
        name: 'email',
        description: 'Адрес электронной почты пользователя',
        nullable: false,
        required: false,
    })
    public email?: string;

    @IsString()
    @IsOptional()
    @IsValidFullname()
    @ApiProperty({
        name: 'fullname',
        description: 'ФИО пользователя в системе',
        required: false,
        writeOnly: true,
    })
    public fullname?: string;

    @IsOptional()
    @IsPhoneNumber()
    @ApiProperty({
        name: 'phone',
        description: 'Номер телефона пользователя',
        required: false,
    })
    public phone?: string;

    @IsString()
    @IsOptional()
    @ApiProperty({
        name: 'address',
        description: 'Адрес пользователя',
        required: false,
    })
    public address?: string;

    @IsString()
    @IsOptional()
    @ApiProperty({
        name: 'city',
        description: 'Город проживания пользователя',
        required: false,
    })
    public city?: string;

    @IsString()
    @IsOptional()
    @ApiProperty({
        name: 'street',
        description: 'Улица проживания пользователя',
        required: false,
        writeOnly: true,
    })
    public street?: string;

    @IsString()
    @IsOptional()
    @ApiProperty({
        name: 'apartment',
        description: 'Номер квартиры пользователя',
        required: false,
        writeOnly: true,
    })
    public apartment?: string;
}
