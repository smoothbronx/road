import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { UserUpdatingDto } from '@/users/dto/updating.dto';
import { RegisterDto } from '@/auth/dto/register.dto';
import { NameSegments } from '@/@types/NameSegments';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@/users/user.entity';
import { Repository } from 'typeorm';
import {
    ConflictException,
    NotFoundException,
    Injectable,
} from '@nestjs/common';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UserEntity)
        private readonly usersRepository: Repository<UserEntity>,
    ) {}

    public getSelfInformation(user: UserEntity): UserEntity {
        return user;
    }

    public createUser(credentials: RegisterDto): Promise<UserEntity> {
        return this.usersRepository.create(credentials).save();
    }

    public async updateSelfInformation(
        user: UserEntity,
        credentials: UserUpdatingDto,
    ): Promise<void> {
        let nameCredentials: QueryDeepPartialEntity<UserEntity>;
        if (credentials.fullname) {
            const nameSegments = this.splitFullname(credentials.fullname);
            nameCredentials = {
                firstname: nameSegments.firstname,
                lastname: nameSegments.lastname,
                patronymic: nameSegments.patronymic,
            };
        } else {
            nameCredentials = {
                firstname: user.firstname,
                lastname: user.lastname,
                patronymic: user.patronymic,
            };
        }

        const updatingObject = {
            apartment: credentials.apartment || user.apartment,
            city: credentials.city || user.city,
            phone: credentials.phone || user.phone,
            email:
                credentials.email &&
                credentials.email !== user.email &&
                (await this.validateCredentials(credentials.email))
                    ? credentials.email
                    : user.email,
            street: credentials.street || user.street,
            address: credentials.address || user.address,
        };

        await this.usersRepository.update(
            { id: user.id },
            { ...updatingObject, ...nameCredentials },
        );
    }

    private splitFullname(fullname: string): NameSegments {
        const splitName: string[] = fullname.split(' ');

        return {
            firstname: splitName[1],
            lastname: splitName[0],
            patronymic: splitName.at(2) || null,
        };
    }

    public async getUserByEmail(email: string): Promise<UserEntity> {
        const user = await this.usersRepository.findOneBy({ email });
        if (!user) throw new NotFoundException('User not found');
        return user;
    }

    public async getUserByEmailAndRefresh(
        email: string,
        refresh: string,
    ): Promise<UserEntity> {
        const user = await this.usersRepository.findOneBy({ email, refresh });
        if (!user) throw new NotFoundException('User not found');
        return user;
    }

    public async validateCredentials(
        email?: string,
        nickname?: string,
    ): Promise<boolean> {
        const credentialsRegistered = await this.usersRepository.exist({
            where: [{ email }, { nickname }],
        });

        if (credentialsRegistered) {
            throw new ConflictException('User with this credentials exist');
        }

        return true;
    }
}
