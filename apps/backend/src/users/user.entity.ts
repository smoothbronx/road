import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity('users')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({
        name: 'email',
        type: 'varchar',
        nullable: false,
        unique: true,
    })
    public email: string;

    @Column({
        name: 'nickname',
        type: 'varchar',
        nullable: false,
        unique: true,
    })
    public nickname: string;

    @Column({
        name: 'firstname',
        type: 'varchar',
        nullable: true,
    })
    public firstname: string | null;

    @Column({
        name: 'lastname',
        type: 'varchar',
        nullable: true,
    })
    public lastname: string | null;

    @Column({
        name: 'patronymic',
        type: 'varchar',
        nullable: true,
    })
    public patronymic: string | null;

    @Column({
        name: 'phone',
        type: 'varchar',
        nullable: true,
    })
    public phone: string | null;

    @Column({
        name: 'address',
        type: 'varchar',
        nullable: true,
    })
    public address: string | null;

    @Column({
        name: 'city',
        type: 'varchar',
        nullable: true,
    })
    public city: string | null;

    @Column({
        name: 'street',
        type: 'varchar',
        nullable: true,
    })
    public street: string | null;

    @Column({
        name: 'apartment',
        type: 'varchar',
        nullable: true,
    })
    public apartment: string | null;

    @Exclude({ toPlainOnly: true })
    @Column({
        name: 'password',
        type: 'varchar',
        nullable: false,
    })
    public password: string;

    @Exclude({ toPlainOnly: true })
    @Column({
        name: 'refresh',
        type: 'varchar',
        nullable: true,
    })
    public refresh: string | null;
}
