import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
    @ApiProperty({
        name: 'email',
        example: 'smoothbronx@xenofium.com',
        writeOnly: true,
        required: true,
    })
    public email: string;

    @ApiProperty({
        name: 'password',
        example: 'SomePassword1',
        writeOnly: true,
        required: true,
    })
    public password: string;
}
