import { JwtConfigWorkspace } from '@/config/workspaces/jwt.workspace';
import { CryptoProvider } from '@/shared/crypto/crypto.provider';
import { TokenPayload, AuthTokens } from 'src/@types/Tokens';
import { RegisterDto } from 'src/auth/dto/register.dto';
import { UsersService } from 'src/users/users.service';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'src/users/user.entity';
import { LoginDto } from 'src/auth/dto/login.dto';
import { ConfigType } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';
import {
    UnauthorizedException,
    NotFoundException,
    Injectable,
    Inject,
} from '@nestjs/common';

@Injectable()
export class AuthService {
    private readonly cryptoProvider = CryptoProvider;

    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
        @Inject(JwtService)
        private readonly jwtService: JwtService,
        @Inject(UsersService)
        private readonly usersService: UsersService,
        @Inject(JwtConfigWorkspace.KEY)
        private readonly jwtConfig: ConfigType<typeof JwtConfigWorkspace>,
    ) {}

    public async register(userCredentials: RegisterDto): Promise<void> {
        await this.usersService.validateCredentials(
            userCredentials.email,
            userCredentials.nickname,
        );

        userCredentials.password = this.cryptoProvider.generateHashFromPassword(
            userCredentials.password,
        );

        await this.usersService.createUser(userCredentials);
    }

    public async refreshTokens(refreshToken: string): Promise<AuthTokens> {
        try {
            const token = this.jwtService.verify<TokenPayload>(refreshToken, {
                publicKey: this.jwtConfig.keys.private,
            });

            const user = await this.usersService.getUserByEmailAndRefresh(
                token.email,
                refreshToken,
            );

            return await this.generateNewTokensPair(user);
        } catch {
            throw new UnauthorizedException('Invalid refresh token');
        }
    }

    public async login(userCredentials: LoginDto): Promise<AuthTokens> {
        try {
            const user = await this.usersService.getUserByEmail(
                userCredentials.email,
            );

            await this.validateUserCredentialsOnLogin(user, userCredentials);
            return this.generateNewTokensPair(user);
        } catch (error) {
            throw error instanceof NotFoundException
                ? new UnauthorizedException('Incorrect email')
                : error;
        }
    }

    private async validateUserCredentialsOnLogin(
        targetUser: UserEntity,
        userCredentials: LoginDto,
    ) {
        const isMatch: boolean = this.cryptoProvider.passwordMatch(
            userCredentials.password,
            targetUser.password,
        );

        if (!isMatch) {
            throw new UnauthorizedException('Incorrect password');
        }
    }

    private async generateNewTokensPair(user: UserEntity): Promise<AuthTokens> {
        const tokens: AuthTokens = this.generateNewAuthTokensPair(user);
        await this.updateUserRefreshToken(user, tokens.refreshToken);
        return tokens;
    }

    private generateNewAuthTokensPair(user: UserEntity): AuthTokens {
        const getTokenPayload = (type): Partial<TokenPayload> => ({
            email: user.email,
            type: type,
        });

        return {
            accessToken: this.jwtService.sign(
                {
                    ...getTokenPayload('access'),
                },
                {
                    privateKey: this.jwtConfig.keys.private,
                    expiresIn: '2h',
                    algorithm: 'HS512',
                    issuer: 'road39',
                },
            ),
            refreshToken: this.jwtService.sign(
                {
                    ...getTokenPayload('refresh'),
                },
                {
                    privateKey: this.jwtConfig.keys.private,
                    expiresIn: '12h',
                    algorithm: 'HS512',
                    issuer: 'road39',
                },
            ),
        };
    }

    private updateUserRefreshToken(user: UserEntity, refreshToken: string) {
        user.refresh = refreshToken;
        return user.save();
    }
}
