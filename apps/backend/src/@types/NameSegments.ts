export type NameSegments = {
    firstname: string;
    lastname: string;
    patronymic: string | null;
};
