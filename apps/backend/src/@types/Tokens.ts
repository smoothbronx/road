import { ApiProperty } from '@nestjs/swagger';

export type AuthTokens = {
    accessToken: string;
    refreshToken: string;
};

export type TokenPayload = {
    type: 'access' | 'refresh';
    email: string;
};

export class AuthTokensDto {
    @ApiProperty({ required: true, readOnly: true })
    public accessToken: string;
    @ApiProperty({ required: true, readOnly: true })
    public refreshToken: string;
}
