module.exports = {
  env: {
    node: true,
  },
  root: true,
  plugins: ['prettier', '@typescript-eslint/eslint-plugin'],
  extends: [
    // JS base
    'eslint:recommended',
    // TS
    'plugin:@typescript-eslint/recommended',
    // Prettier
    'prettier',
  ],
  rules: {
    '@typescript-eslint/explicit-member-accessibility': [
      'error',
      {
        accessibility: 'explicit',
        overrides: {
          accessors: 'explicit',
          constructors: 'no-public',
          methods: 'explicit',
          properties: 'off',
          parameterProperties: 'explicit',
        },
      },
    ],
    '@typescript-eslint/no-explicit-any': 'off',
    eqeqeq: 1,
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'spaced-comment': ['error', 'always', { exceptions: ['-', '+'] }],
    'prettier/prettier': [
      'error',
      {
        endOfLine: 'auto',
      },
    ],
  },
};
